#include <stdio.h>
#include <stdlib.h>
#include "minstack.h"

int main()
{
  int data[10] = {82, 97, 77, 61, 29, 13, 56, 39, 48, 9};
  int i;

  MinStack ms;

  minStackInit(&ms, 10);

  for (i = 0; i < 10; i++) {
    printf("push element of minstack is %d\n", data[i]);
    minStackPush(&ms, data[i]);
    printf("min element of minstack is %d\n", minStackMin(&ms));
  }

  printf("\n");
  
  for (i = 0; i < 10; i++) {
    printf("Pop element of minstack is %d\n", minStackPop(&ms));
    printf("min element of minstack is %d\n", minStackMin(&ms));
  }

  minStackDestroy(&ms);

  exit(0);
}
