#include <stdlib.h>

#include "list.h"
#include "stack.h"

/* name: stack_push
 *  Push the data onto the stack
 */
int stack_push(Stack *stack, const void *data)
{
  return list_ins_next(stack, NULL, data);
}

/* name: stack_pop
 *  Pop the data off the stack
 */
int stack_pop(Stack *stack, void **data)
{
  return list_rem_next(stack, NULL, data);
}
