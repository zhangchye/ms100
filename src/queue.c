#include <stdlib.h>

#include "list.h"
#include "queue.h"

/* name: queue_enqueue
 *  Enqueue the data
 */
int queue_enqueue(Queue *queue, const void *data)
{
  return list_ins_next(queue, list_tail(queue), data);
}

/* name: queue_dequeue
    Dequeue the data
 */
int queue_dequeue(Queue *queue, void **data)
{
  return list_rem_next(queue, NULL, data);
}
