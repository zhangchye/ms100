#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define CHAR_NUM 256

char firstSingleChar(char *str)
{
    assert(str != NULL);
    char *ptr = str;
    int flag[CHAR_NUM] = {0};
    while (*ptr != '\0')
	flag[(size_t)*ptr++]++;
    
    for (ptr = str; *ptr != '\0'; ++ptr)
	if (1 == flag[(size_t)*ptr])
	    return *ptr;
    return '\0';	/* this must the one that occurs exact 1 time. */
}

int main(int argc, char *argv[])
{
    char *cstr = "abaccdeff", *str;
    if (argc != 2)
	str = cstr;
    else
	str = argv[1];

    printf("%s\n", str);
    printf("%c\n", firstSingleChar(str));

    exit(0);
}
