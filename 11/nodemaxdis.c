#include <stdio.h>
#include <stdlib.h>

int maxDistance(Node *root)
{
    int depth;
    return helper(root, depth);
}

int helper(Node *root, int &depth)
{
    if (NULL == root) {
	depth = 0;
	return 0;
    }

    int ld, rd;
    int maxleft = helper(root->left, ld);
    if (root->left != NULL)
	++ld;
    int maxright = helper(root->right, rd);
    if (root->right != NULL)
	++rd;
    depth = max(ld, rd);

    return max(maxleft, max(maxright, ld+rd));
}
