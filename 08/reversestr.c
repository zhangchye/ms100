#include <stdio.h>
#include <string.h>

void reverse_str(char *str) {
  char *p;
  if (NULL == str) return;
  
  p = str + strlen(str) - 1;
  while (str < p) {
    *str = *str ^ *p;
    *p = *str ^ *p;
    *str = *str ^ *p;
    ++str;
    --p;
  }
}

int main() {
  char str[] = "Hello World!";
  reverse_str(str);
  printf("%s\n", str);
  return 0;
}
