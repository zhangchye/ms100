#include <stdio.h>
#include <stdlib.h>

int match(char *str, char *pattern) {
  if ('\0' == *pattern) return 1;
  if ('*' == *pattern) {
    do {
      if (match(str++, pattern + 1)) return 1;
    } while (*str != '\0');
    return 0;
  }
  if ('\0' == *str) return 0;
  if (*str == *pattern || '?' == *pattern)
    return match(str+1, pattern + 1);
  return 0;
}

int main() {
  char *str = "Hello world!";
  char *pattern = "He?lo*l";
  printf("%d\n", match(str, pattern));
  return 0;
}
