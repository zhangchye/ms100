#include <stdio.h>
#include <stdlib.h>

typedef struct ListNode_
{
    int key;
    struct ListNode_ *next;
} Node;

Node * lastbutk(Node *head, int k)
{
    if (k < 0) {
	fprintf(stderr, "k < 0\n");
	exit(1);
    }
    Node *p = head, *pk = head;
    for (; k>0; --k) {
	if (pk->next != NULL)	/* 让链表开始从尾指针开始计数 */
	    pk = pk->next;
	else
	    return NULL;
    }

    while (pk->next != NULL)
	p = p->next, pk = pk->next;

    return p;
}

#define LIST_LEN 10
int main(int argc, char *argv[])
{
    int k;
    Node list[LIST_LEN];
    int i;
    Node *pNode;

    for (i = 0; i < LIST_LEN - 1; ++i) {
	list[i].key = i + 1;
	list[i].next = &list[i + 1];
    }
    list[i].key = i + 1;
    list[i].next = NULL;

    if (argc != 2)
	k = 0;
    else
	k = atoi(argv[1]);

    pNode = lastbutk(list, k);
    if (pNode != NULL)
	printf("the last but %d is %d\n", k, pNode->key);
    else
	printf("the last but %d is NULL\n", k);

    exit(0);
}
