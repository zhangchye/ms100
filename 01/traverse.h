#ifndef TRAVERSE_H
#define TRAVERSE_H

#include "bitree.h"
#include "dlist.h"

int inorder(const BiTreeNode *node, DList *list);

#endif
