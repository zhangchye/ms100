#include "bitree.h"
#include "dlist.h"
#include "traverse.h"

int inorder(const BiTreeNode *node, DList *dlist)
{
  /* Load the list with an inorder listing of the tree */
  if (!bitree_is_eob(node)) {
    
    if (!bitree_is_eob(bitree_left(node)))
      if (inorder(bitree_left(node), dlist) != 0)
	return -1;

    static BiTreeNode *prevouse = NULL;
    static BiTreeNode *next = NULL;
    
    if (NULL == dlist->head) {
      dlist->head = (DListElmt *)node;
      prevouse = (BiTreeNode *)dlist->head;
    } else {
      next = (BiTreeNode *)node;
    }

    if (next != NULL) {
      prevouse->right = next;
      next->left = prevouse;
      prevouse = next;
    }
      
    
    if (!bitree_is_eob(bitree_right(node)))
      if (inorder(bitree_right(node), dlist) != 0)
	return -1;

    dlist->tail = (DListElmt *)next;
  }

  return 0;
}
